<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = [
        'title', 'details_id', 'description', 'tags', 'image'
    ];

    public function iso() {
        return $this->hasMany('App\Iso','id','iso_id');
    }
}
