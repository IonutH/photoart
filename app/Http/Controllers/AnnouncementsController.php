<?php

namespace App\Http\Controllers;

use App\Announcements;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class AnnouncementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $announcements = Announcements::orderBy('id', 'desc')->paginate(3);
        return view('back_admin.announcements.index', compact('announcements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $announcements = Announcements::all();
        return view('back_admin.announcements.create', compact('announcements'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $announcements = new Announcements();

        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'description' => 'required',
            'category' => 'required',
        ]);

        $announcements->title = $request->title;
        $announcements->image = $request->image;
        $announcements->description = $request->description;
        $announcements->category = $request->category;

        if($request->hasFile('image')){
            foreach ($request->file('image') as $image){
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/images/backImages/announcements');
                $img = Image::make($image->path());
                $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'. $name);
                $data[] = $name;

                $destinationPath = public_path('/images/frontImages/announcements');
                $image->move($destinationPath, $name);
            }
        }

        $announcements->image = json_encode($data);
        $announcements->save();


        return redirect('/admin/announcements')->with('success', 'Ai adaugat un anunt cu succes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Http\Response
     */
    public function show(Announcements $announcements)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $announcements = Announcements::findOrFail($id);
        return view('back_admin.announcements.edit', compact('announcements'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $announcements = Announcements::findOrFail($id);
        $announcements->title = $request->title;
        $announcements->image = $request->image;
        $announcements->description = $request->description;
        $announcements->category = $request->category;

        if($request->hasFile('image')){
            foreach ($request->file('image') as $image){
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/images/backImages/announcements');
                $img = Image::make($image->path());
                $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'. $name);
                $data[] = $name;

                $destinationPath = public_path('/images/frontImages/announcements');
                $image->move($destinationPath, $name);
            }
        }

        $announcements->image = json_encode($data);
        $announcements->save();
        return redirect('/admin/announcements')->with('success', 'Ai schimbat un anunt cu succes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Announcements  $announcements
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    public function destroy($id)
    {
        $announcements = Announcements::findOrFail($id);
        if(!$announcements){
            return 'Nu avem ce sterge';
        }else{
            $announcements->delete();
        }
        return redirect()->back();
    }
}
