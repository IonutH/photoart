<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::orderBy('id', 'desc')->paginate(3);
        return view('back_admin.blog.index')->with('blog', $blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $blog = Blog::all();
        return view('back_admin.blog.create')->with('blog', $blog);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $blog = new Blog();
        $blog->title = $request->title;
        $blog->subtitle = $request->subtitle;
        $blog->description = $request->description;
        $blog->category = $request->category;

        if($request->hasFile('blog_image')){
            $image = $request->file('blog_image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/blog');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/blog');
            $image->move($destinationPath, $name);
        }

        $blog->blog_image = $name;
        $blog->save();

        return redirect('/admin/blog')->with('success', 'Ai adaugat cu succes un articol');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blogArticle = Blog::find($id);
        return view('Front.includes.blogArticle', compact('blogArticle', 'blogArticle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return view('back_admin.blog.edit')->with('blog', $blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);

        $blog->title = $request->title;
        $blog->subtitle = $request->subtitle;
        $blog->description = $request->description;
        $blog->category = $request->category;

        if($request->hasFile('blog_image')){
            $image = $request->file('blog_image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/blog');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/blog');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('blog_image');
        $name = $image->getClientOriginalName();
        $blog->blog_image = $name;
        $blog->save();

        return redirect('/admin/blog')->with('success', 'Ai actualizat cu succes un articol');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::findOrFail($id);
        if(!$blog){
            return 'nu avem ce sterge';
        }else{
            $blog->delete();
        }
        return redirect()->back();
    }
}
