<?php

namespace App\Http\Controllers;

use App\Courses;
use Illuminate\Http\Request;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Courses::orderBy('id', 'desc')->paginate(3);
        return view('back_admin.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Courses::all();
        return view('back_admin.courses.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $courses = new Courses();

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'video' => 'required',
        ]);

        $courses->title = $request->title;
        $courses->description = $request->description;

        if($request->hasFile('video')){

            $file = $request->file('video');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/videos/';
            $file->move($path, $filename);
        }

        $file = $request->file('video');
        $filename = $file->getClientOriginalName();
        $courses->video = $filename;
        $courses->save();

        return redirect('/admin/courses')->with('success', 'Ati adaugat un video cu succes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function show(Courses $courses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $courses = Courses::findOrFail($id);
        return view('back_admin.courses.edit', compact('courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $courses = Courses::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $courses->title = $request->title;
        $courses->description = $request->description;

        if($request->hasFile('video')){

            $file = $request->file('video');
            $filename = $file->getClientOriginalName();
            $path = public_path().'/videos/';
            $file->move($path, $filename);
        }

        $file = $request->file('video');
        $filename = $file->getClientOriginalName();
        $courses->video = $filename;
        $courses->save();

        return redirect('/admin/courses')->with('success', 'Ati actualizat un video cu succes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $courses = Courses::findOrFail($id);
        if(!$courses){
            return 'nu avem ce sterge';
        }else{
            $courses->delete();
        }

        return redirect()->back();
    }
}
