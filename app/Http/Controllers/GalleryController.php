<?php

namespace App\Http\Controllers;

use App\Camera;
use App\Diaphragm;
use App\Exposure;
use App\Gallery;
use App\Iso;
use App\Lens;
use App\PhotoDetails;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = Gallery::orderBy('id', 'desc')->paginate(3);

        return view('back_admin.gallery.index', compact('gallery'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $gallery = Gallery::all();
        $iso = Iso::pluck('iso', 'id')->all();
        $camera = Camera::pluck('camera', 'id')->all();
        $exposure = Exposure::pluck('exposure', 'id')->all();
        $lens = Lens::pluck('lens', 'id')->all();
        $diaphragm = Diaphragm::pluck('diaphragm', 'id')->all();
        return view('back_admin.gallery.create')->with([
            'gallery' => $gallery,
            'iso' => $iso,
            'camera' => $camera,
            'exposure' => $exposure,
            'lens' => $lens,
            'diaphragm' => $diaphragm,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function store(Request $request){
        $gallery = new Gallery();
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'required',
            'tags' => 'required',
        ]);

        $gallery->title = $request->title;
        $gallery->description = $request->description;
        $gallery->tags = $request->tags;
        $gallery->iso_id = $request->iso_id;
        $gallery->camera_id = $request->camera_id;
        $gallery->exposure_id = $request->exposure_id;
        $gallery->lens_id = $request->lens_id;
        $gallery->diaphragm_id = $request->diaphragm_id;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/gallery');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/gallery');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $gallery->image = $name;
        $gallery->save();

        return redirect('/admin/gallery')->with('success', 'Ati adaugat o noua fotografie');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(Gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        $iso = Iso::pluck('iso', 'id')->all();
        $camera = Camera::pluck('camera', 'id')->all();
        $exposure = Exposure::pluck('exposure', 'id')->all();
        $lens = Lens::pluck('lens', 'id')->all();
        $diaphragm = Diaphragm::pluck('diaphragm', 'id')->all();
        return view('back_admin.gallery.edit')->with([
            'gallery' => $gallery,
            'iso' => $iso,
            'camera' => $camera,
            'exposure' => $exposure,
            'lens' => $lens,
            'diaphragm' => $diaphragm,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $gallery = Gallery::findOrFail($id);
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'tags' => 'required',
        ]);

        $gallery->title = $request->title;
        $gallery->description = $request->description;
        $gallery->tags = $request->tags;
        $gallery->iso_id = $request->iso_id;
        $gallery->camera_id = $request->camera_id;
        $gallery->exposure_id = $request->exposure_id;
        $gallery->lens_id = $request->lens_id;
        $gallery->diaphragm_id = $request->diaphragm_id;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/gallery');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/gallery');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $gallery->image = $name;
        $gallery->save();

        return redirect('/admin/gallery')->with('success', 'Ati actualizat fotografia');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        if(!$gallery){
            return 'nu avem ce sterge';
        }else{
            $gallery->delete();
        }

        return redirect()->back();
    }

}
