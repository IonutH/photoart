<?php

namespace App\Http\Controllers;

use App\Camera;
use App\Diaphragm;
use App\Exposure;
use App\Iso;
use App\Lens;
use App\PhotoDetails;
use Illuminate\Http\Request;

class PhotoDetailsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $details = Iso::orderBy('id', 'desc')->paginate(3, ['*'], 'details');
        $details1 = Camera::orderBy('id', 'desc')->paginate(3, ['*'], 'details1');
        $details2 = Exposure::orderBy('id', 'desc')->paginate(3, ['*'], 'details2');
        $details3 = Lens::orderBy('id', 'desc')->paginate(3, ['*'], 'details3');
        $details4 = Diaphragm::orderBy('id', 'desc')->paginate(3, ['*'], 'details4');

//        $details5 = $details + $details1 + $details2 + $details3 + $details4;
//        $details5 = $details + $details1;

        return view('back_admin.technical_details.index', compact('details', 'details1', 'details2', 'details3', 'details4'));
//        return view('back_admin.technical_details.index', compact('details5'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $details = Iso::all();
        $details1 = Camera::all();
        $details2 = Exposure::all();
        $details3 = Lens::all();
        $details4 = Diaphragm::all();
        return view('back_admin.technical_details.create', compact('details', 'details1', 'details2', 'details3', 'details4'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'iso' => 'required',
            'camera' => 'required',
            'exposure' => 'required',
            'lens' => 'required',
            'diaphragm' => 'required'
        ]);

        $details = new Iso();
        $details->iso = $request->iso;
        $details->save();

        $details1 = new Camera();
        $details1->camera = $request->camera;
        $details1->save();

        $details2 = new Exposure();
        $details2->exposure = $request->exposure;
        $details2->save();

        $details3 = new Lens();
        $details3->lens = $request->lens;
        $details3->save();

        $details4 = new Diaphragm();
        $details4->diaphragm = $request->diaphragm;
        $details4->save();

        return redirect('/admin/details')->with('success', 'Detaliile despre poza au fost adaugate cu succes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PhotoDetails  $photoDetalies
     * @return \Illuminate\Http\Response
     */
    public function show(PhotoDetails $photoDetalies)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PhotoDetails  $photoDetalies
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $details = Iso::findOrFail($id);

        return view('back_admin.technical_details.edit', compact('details'));
    }

    public function edit1($id)
    {
        $details1 = Camera::findOrFail($id);

        return view('back_admin.technical_details.edit1', compact( 'details1'));
    }

    public function edit2($id)
    {
        $details2 = Exposure::findOrFail($id);

        return view('back_admin.technical_details.edit2', compact('details2'));
    }

    public function edit3($id)
    {
        $details3 = Lens::findOrFail($id);

        return view('back_admin.technical_details.edit3', compact('details3'));
    }

    public function edit4($id)
    {
        $details4 = Diaphragm::findOrFail($id);

        return view('back_admin.technical_details.edit4', compact( 'details4'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PhotoDetails  $photoDetalies
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $details = Iso::findOrFail($id);
        $details->iso = $request->iso;
        $details->save();

        $this->validate($request, [
            'iso' => 'required',

        ]);

        return redirect('/admin/details')->with('success', 'Detaliile despre poza au fost actualizate cu succes');
    }

    public function update1(Request $request, $id)
    {

        $details1 = Camera::findOrFail($id);
        $details1->camera = $request->camera;
        $details1->save();

        $this->validate($request, [
            'camera' => 'required',
        ]);

        return redirect('/admin/details')->with('success', 'Detaliile despre poza au fost actualizate cu succes');
    }
    public function update2(Request $request, $id)
    {

        $details2 = Exposure::findOrFail($id);
        $details2->exposure = $request->exposure;
        $details2->save();;

        $this->validate($request, [
            'exposure' => 'required',
        ]);

        return redirect('/admin/details')->with('success', 'Detaliile despre poza au fost actualizate cu succes');
    }
    public function update3(Request $request, $id)
    {

        $details3 = Lens::findOrFail($id);
        $details3->lens = $request->lens;
        $details3->save();

        $this->validate($request, [
            'lens' => 'required',
        ]);

        return redirect('/admin/details')->with('success', 'Detaliile despre poza au fost actualizate cu succes');
    }
    public function update4(Request $request, $id)
    {

        $details4 = Diaphragm::findOrFail($id);
        $details4->diaphragm = $request->diaphragm;
        $details4->save();

        $this->validate($request, [
            'diaphragm' => 'required',
        ]);

        return redirect('/admin/details')->with('success', 'Detaliile despre poza au fost actualizate cu succes');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PhotoDetails  $photoDetalies
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $details = Iso::findOrFail($id);
        $details->delete();

        return redirect()->back();
    }

    public function destroy1($id)
    {
        $details1 = Camera::findOrFail($id);
        $details1->delete();

        return redirect()->back();
    }
    public function destroy2($id)
    {
        $details2 = Exposure::findOrFail($id);
        $details2->delete();

        return redirect()->back();
    }
    public function destroy3($id)
    {
        $details3 = Lens::findOrFail($id);
        $details3->delete();

        return redirect()->back();
    }
    public function destroy4($id)
    {
        $details4 = Diaphragm::findOrFail($id);
        $details4->delete();

        return redirect()->back();
    }
}
