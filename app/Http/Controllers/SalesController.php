<?php

namespace App\Http\Controllers;

use App\Sales;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sales = Sales::orderBy('id', 'desc')->paginate(3);
        return view('back_admin.sales.index',compact('sales'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sales = Sales::all();
        return view('back_admin.sales.create', compact('sales'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sales = new Sales();

        $this->validate($request, [
            'price' => 'required',
            'description' => 'required',
            'image' => 'required',
            'category' => 'required',
        ]);

        $sales->description = $request->description;
        $sales->category = $request->category;
        $sales->price = $request->price;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/sales');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/sales');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $sales->image = $name;
        $sales->save();

        return redirect('/admin/sales')->with('success', 'Ati adaugat o noua vanzare');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function show(Sales $sales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sales = Sales::findOrFail($id);
        return view('back_admin.sales.edit', compact('sales'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sales = Sales::findOrFail($id);
        $this->validate($request, [
            'price' => 'required',
            'description' => 'required',
            'category' => 'required',
        ]);

        $sales->description = $request->description;
        $sales->category = $request->category;
        $sales->price = $request->price;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/sales');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/sales');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $sales->image = $name;
        $sales->save();

        return redirect('/admin/sales')->with('success', 'Ati adaugat o noua vanzare');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sales = Sales::findOrFail($id);
        if(!$sales){
            return 'nu avem ce sterge';
        }else{
            $sales->delete();
        }

        return redirect()->back();
    }
}
