<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Gallery;
use App\Sales;
use App\WeeklyPhoto;
use App\Workshop;
use App\WeeklyChallenge;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class WebController extends Controller
{
    public function getAllImages(){
        $gallery = Gallery::all();
        return view('Front.includes.gallery', compact('gallery'));
    }

    public function increaseLike(Request $request)
    {
        $selectedGallery = Gallery::where('id', '=', $request->id)->first();
        $selectedGallery->likes += 1;
        $selectedGallery->save();
    }

    public function increaseView(Request $request)
    {
        $selectedGallery = Gallery::where('id', '=', $request->id)->first();
        $selectedGallery->views += 1;
        $selectedGallery->save();
    }

    public function increaseVoteChallenge(Request $request)
    {
        $selectedGallery = WeeklyPhoto::where('id', '=', $request->id)->first();
        $selectedGallery->votes += 1;
        $selectedGallery->save();
    }


    public function paginateSales(){
        $sales = Sales::paginate(6);
        return view('Front.includes.sales', compact('sales'));
    }

    public function paginateBlog(){
        $blog = Blog::orderBy('created_at', 'DESC')->paginate(2);
        return view('Front.includes.blog', compact('blog'));
    }

    public function paginateWorkshos(){
        $workshops = Workshop::orderBy('created_at', 'DESC')->paginate(2);
        return view('Front.includes.workshop', compact('workshops'));
    }

    public function increaseWorkshopParticipate(Request $request)
    {
        $selectedWorkshop = Workshop::where('id', '=', $request->id)->first();
        $selectedWorkshop->count += 1;
        $selectedWorkshop->save();
    }

    public function getCurentWeeklyChallenge(){
        $curentWeeklyChallenge = WeeklyChallenge::latest()->first();
        $weeklyPhoto = WeeklyPhoto::all();
        return view('Front.includes.weekly')->with(['curentWeeklyChallenge'=> $curentWeeklyChallenge, 'weeklyPhoto'=> $weeklyPhoto]);
    }


    public function uploadWeekly(Request $request){
        $photoWeekly = new WeeklyPhoto();
        $photoWeekly->title = $request->title;
        $photoWeekly->description = $request->description;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/weeklyPhoto');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/weeklyPhoto');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $photoWeekly->image = $name;
        $photoWeekly->save();

        return redirect('/weekly')->with('success', 'Ati adaugat o noua fotografie');
    }

    public function contactUs(Request $request){
        return redirect('/intro')->with('success', 'Ati trimis mesajul!');
    }

    public function searchBlog($search){
        $blog = Blog::where([['title', 'LIKE', '%' . $search . '%']])->orWhere([['subtitle', 'LIKE', '%' . $search . '%']])->orWhere([['category', 'LIKE', '%' . $search . '%']])->paginate(2);
        
        return view('Front.includes.blogSearch')->with('blog', $blog);
    }
}
