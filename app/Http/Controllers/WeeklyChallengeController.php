<?php

namespace App\Http\Controllers;

use App\WeeklyChallenge;
use App\WeeklyPhoto;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class WeeklyChallengeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $weekly = WeeklyChallenge::orderBy('id', 'desc')->paginate(3);
        $photos = WeeklyPhoto::orderBy('id', 'desc')->paginate(3);
        return view('back_admin.challenges.index', compact('weekly', 'photos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function create()
    {
        $weekly = WeeklyChallenge::all();
        return view('back_admin.challenges.create', compact('weekly'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $weekly = new WeeklyChallenge();

        $weekly->title = $request->title;
        $weekly->description = $request->description;

        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'description' => 'required',
        ]);

        if($request->hasFile('image')){
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/images/backImages/weekly');
                $img = Image::make($image->path());
                $img->resize(200, 200, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/weekly');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $weekly->image = $name;
        $weekly->save();

//        if($photo = $request->file(['image'])) {
//
//            $name = time() . $photo->getClientOriginalName();
//            $photo->move(public_path('/images/backImages/challenges'), $name);
//            $input['image'] = $name;
//        }

//        if($request->hasFile('image')){
//            foreach ($request->file('image') as $image){
//                $name = $image->getClientOriginalName();
//                $destinationPath = public_path('/images/backImages/announcements');
//                $img = Image::make($image->path());
//                $img->resize(200, 200, function ($constraint) {
//                    $constraint->aspectRatio();
//                })->save($destinationPath.'/'. $name);
//                $data[] = $name;
//            }
//        }


//        if($photo = $request->file(['image'])) {
//
//            $name = time() . $photo->getClientOriginalName();
//
//            $destinationPath = public_path('/images/backImages/challenges/');
//            $img = Image::make($photo->path());
//            $img->resize(300, 300, function ($constraint) {
//                $constraint->aspectRatio();
//            })->save($destinationPath.'/'.$input['image']);
//
//            $destinationPath = public_path('/images/backImages/challenges/');
//            $photo->move($destinationPath, $input['image']);
//
//            $input['image'] = $name;
//        }

//        $weekly->create($input);

        return redirect('/admin/challenges')->with('success', 'Ai adaugat un challenge cu succes');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WeeklyChallenge  $weeklyChallege
     * @return \Illuminate\Http\Response
     */
    public function show(WeeklyChallenge $weeklyChallege)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WeeklyChallenge  $weeklyChallege
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function edit($id)
    {
        $weekly = WeeklyChallenge::find($id);
        return view('back_admin.challenges.edit', compact('weekly', 'weekly'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WeeklyChallenge  $weeklyChallege
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $weekly = WeeklyChallenge::findOrFail($id);

        $weekly->title = $request->title;
        $weekly->description = $request->description;

        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'description' => 'required',
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/weekly');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/weekly');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $weekly->image = $name;
        $weekly->save();

        return redirect('/admin/challenges')->with('success', 'Ai adaugat un challenge cu succes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WeeklyChallenge  $weeklyChallege
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    public function destroy($id)
    {
        $weekly = WeeklyChallenge::findOrFail($id);
        if (!$weekly){
            return 'nu avem ce sterge';
        }else{
            $weekly->delete();
        }
        return redirect()->back();
    }

    public function destroyPhoto($id)
    {
        $photo = WeeklyPhoto::findOrFail($id);
        if (!$photo){
            return 'nu avem ce sterge';
        }else{
            $photo->delete();
        }
        return redirect()->back();
    }
}
