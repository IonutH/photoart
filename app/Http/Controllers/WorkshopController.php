<?php

namespace App\Http\Controllers;

use App\Workshop;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class WorkshopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workshop = Workshop::orderBy('id', 'desc')->paginate(3);
        return view('back_admin.workshop.index', compact('workshop'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $workshop = Workshop::all();
        return view('back_admin.workshop.create', compact('workshop'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workshop = new Workshop();

        $this->validate($request, [
            'image' => 'required',
            'title' => 'required',
            'description' => 'required',
            'date' => 'required'
        ]);

        $workshop->title = $request->title;
        $workshop->description = $request->description;
        $workshop->date = $request->date;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/workshop');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/workshop');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $workshop->image = $name;
        $workshop->save();

        return redirect('/admin/workshop')->with('success', 'Ati adaugat un workshop');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function show(Workshop $workshop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $workshop = Workshop::findOrFail($id);
        return view('back_admin.workshop.edit', compact('workshop'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $workshop = Workshop::findOrFail($id);
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'date' => 'required'
        ]);

        $workshop->title = $request->title;
        $workshop->description = $request->description;
        $workshop->date = $request->date;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/images/backImages/workshop');
            $img = Image::make($image->path());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath.'/'. $name);

            $destinationPath = public_path('/images/frontImages/workshop');
            $image->move($destinationPath, $name);
        }

        $image = $request->file('image');
        $name = $image->getClientOriginalName();
        $workshop->image = $name;
        $workshop->save();

        return redirect('/admin/workshop')->with('success', 'Ati actualizat un workshop');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Workshop  $workshop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workshop = Workshop::findOrFail($id);
        if(!$workshop){
            return 'nu avem ce sterge';
        }else{
            $workshop->delete();
        }

        return redirect()->back();
    }
}
