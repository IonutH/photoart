<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Iso extends Model
{
    protected $fillable = [
      'iso'
    ];

    public function gallery(){
        return $this->belongsTo('App\Gallery','id','iso_id');
    }
}
