<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoDetails extends Model
{
    protected $fillable = [
        'iso', 'camera', 'exposure', 'lens', 'diaphragm'
    ];

    public function gallery(){
        return $this->belongsTo('App\Gallery','id','details_id');
    }
}
