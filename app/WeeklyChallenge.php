<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeeklyChallenge extends Model
{
    protected $fillable = [
        'title', 'description', 'image',
    ];
}
