<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeeklyPhoto extends Model
{
    protected $fillable = [
        'title', 'description', 'image','votes',
    ];
}
