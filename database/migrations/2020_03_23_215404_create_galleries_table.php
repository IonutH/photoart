<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->id();
            $table->text('user_id')->nullable();
            $table->text('iso_id')->nullable();
            $table->text('camera_id')->nullable();
            $table->text('exposure_id')->nullable();
            $table->text('lens_id')->nullable();
            $table->text('diaphragm_id')->nullable();
            $table->text('image')->nullable();
            $table->string('votes')->default(0);
            $table->string('views')->default(0);
            $table->string('likes')->default(0);
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('tags')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');
    }
}
