$(".likes").on("click", function () {
    event.preventDefault();
    let id = this.id;

    $.ajax({
        data: { id },
        type: "GET",
        url: '/increaseLike',
        cache: false,
        success: function (data) {
            //  location.reload();
        },
        error: function (err) {
            console.log('error increaseLike');
            console.log(err);
        }
    });
});

$(".votes-weekly").on("click", function () {
    event.preventDefault();
    let id = this.id;

    $.ajax({
        data: { id },
        type: "GET",
        url: '/increaseVoteChallenge',
        cache: false,
        success: function (data) {
            //  location.reload();
        },
        error: function (err) {
            console.log('error increaseVoteChallengesss');
            console.log(err);
        }
    });
});


$(".increaseWorkshop").on("click", function () {
    event.preventDefault();
    let id = this.id;

    $.ajax({
        data: { id },
        type: "GET",
        url: '/increaseWorkshop',
        cache: false,
        success: function (data) {
            location.reload();
        },
        error: function (err) {
            console.log('error increaseWorkshop');
            console.log(err);
        }
    });
});

$("#blogSearch").keyup(function (event) {
    if (event.keyCode === 13) {
        let searchString = event.target.value;
        if (searchString.length > 2) {
            $.ajax({
                url: "/blog/searchBlog/" + searchString,
                method: "GET",
                data: {},
                dataType: "text",
                success: function (data) {
                    $('#blogSearchResults').html(data).fadeIn('slow');
                }, error: function (err) {
                    console.log('error blogSearch');
                    console.log(err);
                }
            });
        }
    }
});


$(function ($) {
    $(document).on('click', '.lightboxgallery-gallery-item', function (event) {
        event.preventDefault();
        $(this).lightboxgallery({
            showCounter: true,
            showTitle: true,
            showDescription: true
        });
    });
});