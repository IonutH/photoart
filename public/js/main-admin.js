//Metis Menu

$('#metismenu').metisMenu({
    toggle: false
});

//Calendar
$(document).ready(function(){
    $('#datePick').multiDatesPicker({
        dateFormat: "dd-mm-yy",
    });
});

// confirm to delete
$('.btn-danger').on('click', function () {
    return confirm("Ești sigur că vrei să ștergi?");
});

//swtich

$('.switch-button').click(function () {
   $(this).toggleClass('switch-active');
});
