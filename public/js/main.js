// Change anchor color on active page
$(document).ready(function () {
    $(".blue-class").each(function () {
        if (this.href == window.location.href) {
            $(this).addClass("blue-anchor");
        }
    });
    $(".green-class").each(function () {
        if (this.href == window.location.href) {
            $(this).addClass("green-anchor");
        }
    });
    $(".red-class").each(function () {
        if (this.href == window.location.href) {
            $(this).addClass("red-anchor");
        }
    });
    $(".orange-class").each(function () {
        if (this.href == window.location.href) {
            $(this).addClass("orange-anchor");
        }
    });
});

//Add background-color to nav
if (window.matchMedia("(min-width: 1200px)").matches) {
    $(window).scroll(function () {
        let navigation = $('#navigation');
        let currentScroll = $(window).scrollTop();
        // let mainView = $('.main-view').offset().top;
        if (currentScroll >= 300) {
            navigation.addClass('nav-white');
        } else {
            navigation.removeClass('nav-white');
        }
        if (window.matchMedia("(min-width: 3500px)").matches) {
            if (currentScroll >= 450) {
                navigation.addClass('nav-white');
            } else {
                navigation.removeClass('nav-white');
            }
        }

    });
}

//Responsive nav
let hamburger = $('.hamburger-menu');
$(hamburger).click(function () {
    $(hamburger).toggleClass("nav-open");
    $(".hamburger").toggleClass("is-active");
    $("#navigation .nav-group ul").toggleClass('ul-active');
});

if (window.matchMedia("(max-width: 1200px)").matches) {
    $(window).scroll(function () {
        let scroll = $(window).scrollTop();

        if (scroll >= 10) {
            $(hamburger).addClass("is-scroll");
        } else {
            $(hamburger).removeClass("is-scroll");
        }
    });
}


//Homepage carousel

$('#homepage-carousel').carousel({
    interval: 5000,
    pause: false
});


//FAQ

$(document).ready(function () {
    $(".box > .question").on("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(this)
                .siblings(".answer")
                .slideUp();
            $(".box > .question i")
                .removeClass("fa-minus")
                .addClass("fa-plus");
        } else {
            $(".box > .question i")
                .removeClass("fa-minus")
                .addClass("fa-plus");
            $(this)
                .find("i")
                .removeClass("fa-plus")
                .addClass("fa-minus");
            $(".box > .question").removeClass("active");
            $(this).addClass("active");
            $(".answer").slideUp();
            $(this)
                .siblings(".answer")
                .slideDown();
        }
    });
});

// AOS
AOS.init();

//CONTACT POP UP

$("#contact .alert-success").delay(5000).fadeOut("slow");

//Remove is-danger class while typing

$(document).ready(function () {
    $('#name').on('keypress', function () {
        if ($(this).val().length > 0) {
            $('#name').removeClass('is-danger');
        }
        $('#contact .alert-danger').fadeOut();
    });

    $('#email').on('keypress', function () {
        if ($(this).val().length > 0) {
            $('#email').removeClass('is-danger');
        }
        $('#contact .alert-danger').fadeOut();
    });

    $('#subject').on('keypress', function () {
        if ($(this).val().length > 0) {
            $('#subject').removeClass('is-danger');
        }
        $('#contact .alert-danger').fadeOut();
    });

    $('#message').on('keypress', function () {
        if ($(this).val().length > 0) {
            $('#message').removeClass('is-danger');
        }
        $('#contact .alert-danger').fadeOut();
    });
});
