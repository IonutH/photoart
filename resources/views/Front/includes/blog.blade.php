@extends('Front.layouts.index')

@section('content')

<!-- Main -->
<article id="main" class="blog">
    <header>
        <h2>Blog</h2>
        <p>Nu rata ultimele noutati</p>
    </header>
    <section class="wrapper style5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-3 text-center">

                    <div class="card" style="width: 18rem;">
                        <div class="card-header">
                            Search
                        </div>
                        <ul class="list-group list-group-flush">
                            <input id="blogSearch" type="text" placeholder="Search" aria-label="Search">
                        </ul>
                    </div>

                    <div class="card mt-3" style="width: 18rem;">
                        <div class="card-header">
                            Category
                        </div>
                        <ul class="list-group list-group-flush">
                            @foreach($blog as $item)
                            <li class="list-group-item">{{ $item->category }}</li>
                            @endforeach

                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-9" id="blogSearchResults">
                    @include('Front.includes.blogSearch')
                </div>
            </div>
        </div>
    </section>
</article>

@endsection