@extends('Front.layouts.index')

@section('content')

<!-- Main -->
<article id="main" class="blog-article">
    <header>
        <h2>{{$blogArticle->title}}</h2>
        <p>{{$blogArticle->subtitle}}</p>
    </header>
    <section class="wrapper style5">
        <div class="container-fluid">
            <div class="row">
                <div class="col 12">
                    <p><span class="image left"><img src="{{asset('images/backImages/weekly/' . $blogArticle->blog_image)}}" alt="" /></span> {{$blogArticle->description}}  </p>
                    <a href="{{route('blog')}}" class="btn btn-primary">Înapoi</a>
                </div>
            </div>
        </div>
    </section>
</article>

@endsection