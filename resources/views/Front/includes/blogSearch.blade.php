@if ($blog->count() !=0)
@foreach($blog as $item)
<div class="col-12">
    <div class="card mb-5">
        <div class="card-body">
            <figure class="card-img-top">
                <a href="{{asset('images/frontImages/blog/' . $item->blog_image)}}" data-size="1600x1067" id="{{ $item-> id }}">

                    <img alt="picture" src="{{asset('images/frontImages/blog/' . $item->blog_image)}}" class="img-fluid" />
                </a>
            </figure>
        </div>
        <div class="card-body">
            <h4 class="card-title">{{ $item-> title }}</h4>
            <hr>
            <h6 class="mt-3">{{ $item->subtitle }}</h6>
            <p class="card-text mt-3">
                {{ Str::limit($item-> description, $limit = 300, $end = '...') }}
            </p>
            <a href="{{route('blogArticle.show', $item->id)}}" class="btn btn-primary">Read article</a>
        </div>
        <div class="card-header">
            Posted on {{date('d-m-Y', strtotime($item->created_at))}}
        </div>
    </div>
</div>
@endforeach
<div class="col-12 justify-content-center d-flex">
    {{ $blog->links() }}
</div>
@else
<div class="col-12 text-center mt-5">
    <p>Ne pare rau, nu s-au gasit articole conform cautarii alese!</p>
</div>
@endif