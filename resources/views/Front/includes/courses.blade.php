@extends('Front.layouts.index')

@section('content')

<!-- Main -->
<article id="main" class="courses">
    <header>
        <h2>Cursuri</h2>
        <p>Aici poti gasi materiale pentru a te dezvolta ca fotograf</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="row">
                <div class="col-md-6">

                    <div class="card" style="margin:auto;">
                        <iframe height="315" src="https://www.youtube.com/embed/9o-2ef2bfvQ" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="card-body">
                            <h4 class="card-title">Learn Photography - Simple, Practical </h4>
                            <p class="card-text">First video in free 10-part photography course. How I got to shoot for Australian Geographic Magazine, travel the world and
                                sell
                                wildlife photos and videos to Discovery and National Geographic channel etc. It's just a bit about me, my background and what you're in for
                                over the
                                next 9 videos</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="card" style="margin:auto;">
                        <iframe height="315" src="https://www.youtube.com/embed/LxO-6rlihSg" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="card-body">
                            <h4 class="card-title">Australian Geographic Photographer - Chris Bray</h4>
                            <p class="card-text">Get off 'Auto Mode' and unlock your creative potential! This popular, complete course by award-winning photographer Chris Bray
                                uses simple explanations, plenty of examples and even practical sessions to guide you through from basic setup and composition to aperture,
                                shutter speed, exposure, ISO, lighting, lenses, histograms, white balance and more!</p>
                        </div>
                    </div>

                </div>
            </div>
            <hr />
            <div class="col-12 mt-5">
                <h3>Staying Ahead of the Curve: The Importance of Photography Education</h3>
                <p>The age-old adage goes: “It’s never too late to learn.” The saying is especially true in photography, a field that’s a breeding ground for rapid
                    advancements. Even for professionals, instruction can be vital at all stages of a photographer’s career. New York Institute of Photography (NYIP) student
                    advisor George Delgado points to the fact that photography is both timeless and in constant flux, particularly with technology evolving at so fast a pace.
                </p>

                <p>Licensed by the New York State Department of Education, the New York Institute of Photography is largest online photography school in the world. After more
                    than a century of training photographers, the reach and influence of this venerable institution is now global thanks to the modern-day luxury of online
                    education. The school’s most popular program, the Complete Course in Professional Photography is widely considered to be the gold standard for a
                    well-rounded photographic education by many in the image-making industry today.</p>

                

                <p>NYIP lessons incorporate audio, video and reading assignments—accessed in an online learning center—with exams and photo projects judiciously reviewed by
                    professional photographers. While the courses are delivered using Internet technology, all students benefit from the personal mentoring and assistance
                    provided by licensed instructors—professional photographers themselves—via email and telephone.</p>
                <p>Chris Corradino, a professional photographer based in New York City, is NYIP’s faculty director as well as an instructor. He’s also a graduate of the
                    program. With his own business specializing in photojournalism, travel and editorial photography—some of his recent credits include work published by the
                    Associated Press, National Geographic, The New Yorker, and the Wall Street Journal—Corradino knows firsthand the value of NYIP’s curriculum. “Even if you
                    are already comfortable with the technical aspects of photography, the program covers a wide array of topics,” he says. “The teachers provide personalized
                    evaluations full of useful information you can take into the field with you.”</p>
                <p>While the curriculum is designed to start with the basics of any subject and build upon skills as they develop, many photographers who are already working
                    in the field sign up for courses in order to keep ahead of the curve in their ever-changing profession. Delgado too was an NYIP student before joining the
                    staff. He first enrolled in order to find out if he could match his lifelong enthusiasm for photography with the skills needed to pursue a career as a
                    professional. Years later, he credits the comprehensive education he got from NYIP for the success of his New York City-based business of portrait
                    photography.</p>

                <p>A formally structured curriculum, such as the Complete Course in Professional Photography, is an invaluable means for gaining the in-depth knowledge and
                    skills needed for a lucrative career in photography. In addition, an NYIP Graduation Certificate serves as an important professional credential.</p>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-6">

                    <div class="card" style="margin:auto;">
                        <iframe height="315" src="https://www.youtube.com/embed/xHvFHRPLvII" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="card-body">
                            <h4 class="card-title">6 Mobile Photography Tips you must know </h4>
                            <p class="card-text">In this video, Pixel Viilage is giving you 6 tips which anyone can practice and improve ones photography with mobile phones.</p>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">

                    <div class="card" style="margin:auto;">
                        <iframe height="315" src="https://www.youtube.com/embed/WXdAX0No2hM" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <div class="card-body">
                            <h4 class="card-title">Be a Better Photographer in 45 mins</h4>
                            <p class="card-text">A simple straight forward video tutorial to help you with your basics of photography.!</p>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>
</article>

@endsection