@extends('Front.layouts.index')

@section('content')

<!-- Main -->
<article id="main" class="gallery">
    <header>
        <h2>Galerie</h2>
        <p>Nu am încredere în cuvinte. Am încredere în poze!</p>
    </header>
    <section class="wrapper style5">
        <div class="container">
            <div class="lightboxgallery-gallery clearfix">
                @foreach($gallery as $item)
                <a class="lightboxgallery-gallery-item" id="{{$item-> id}}" target="_blank" href="{{asset('images/frontImages/gallery/' . $item->image)}}"
                    data-title="{{$item->title}}" data-alt="{{$item->title}}" data-desc="{{$item->description}}">
                    <div>
                        <img src="{{asset('images/frontImages/gallery/' . $item->image)}}" title="{{$item->title}}" alt="{{$item->title}}">
                        <div class="lightboxgallery-gallery-item-content">
                            <span class="lightboxgallery-gallery-item-title">
                                <div class="card-details">
                                    <div class="views">
                                        {{ $item->views }} <i class="fas fa-glasses" aria-hidden="true"></i>
                                    </div>
                                    <div class="likes" id="{{ $item-> id }}">
                                        <span id="{{ $item-> id }}"> {{ $item->likes }} </span>
                                        <i class="fa fa-heart" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </section>
</article>

@endsection