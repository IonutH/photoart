@extends('Front.layouts.index')

@section('content')
<!-- Banner -->
<section id="banner">
    <div class="inner">
        <h2>Photo Art</h2>
        <p>Photo's with style</p>
        {{-- <p>Another fine responsive<br /> site template freebie<br /> crafted by <a href="http://html5up.net">HTML5 UP</a>.</p>
                <ul class="actions special">
                    <li><a href="#" class="button primary">Activate</a></li>
                </ul> --}}
    </div>
    <a href="#one" class="more scrolly">Learn More
        <br><i class="fas fa-arrow-down"></i>
    </a>

</section>

<!-- One -->
<section id="one" class="wrapper style1 special video">
    <div class="inner">
        <header class="major">
            <h2>Asta e șansa ta!</h2>
            <h1><a href="https://www.youtube.com/embed/9o-2ef2bfvQ" class="youtube-media"><i class="fas fa-play-circle"></i> Watch the
                    Video</a></h1>
            {{-- <p>Aliquam ut ex ut augue consectetur interdum. Donec amet imperdiet eleifend<br /> fringilla tincidunt. Nullam dui leo Aenean mi ligula, rhoncus ullamcorper.</p> --}}
        </header>
{{--         <ul class="icons major">--}}
{{--                    <li><span class="icon fa-gem major style1"><span class="label">Lorem</span></span>--}}
{{--                    </li>--}}
{{--                    <li><span class="icon fa-heart major style2"><span class="label">Ipsum</span></span>--}}
{{--                    </li>--}}
{{--                    <li><span class="icon solid fa-code major style3"><span class="label">Dolor</span></span>--}}
{{--                    </li>--}}
{{--                </ul>--}}
    </div>
</section>

<!-- Two -->
 <section id="two" class="wrapper alt style2">
    <section class="spotlight">
        <div class="image"><img src="{{ '/img/pic001.jpg' }}" alt="" /></div>
        <div class="content">
            <h2>Bucuria de a învăța lucruri noi<br /></h2>
            <p>Avem plăcerea să te ajutăm să înveți lucruri pe care te vor ajuta în cariera ta de fotograf. Suntem aici pentru tine și vrem să te ajutăm cum putem mai bine!</p>
        </div>
    </section>
    <section class="spotlight">
        <div class="image"><img src="{{ '/img/pic002.jpg' }}" alt="" /></div>
        <div class="content">
            <h2>O comunitate unită!<br /></h2>
            <p>Dorim să formăm cea mai mare și cea mai frumoasă comunitate de fotografi din România. Vin-o în echipa noastră astăzi. Nu vei regreta! Vrem să-ți vedem creațiile</p>
        </div>
    </section>
    <section class="spotlight">
        <div class="image"><img src="{{ '/img/pic004.jpg' }}" alt="" /></div>
        <div class="content">
            <h2>Concursuri săptămânale<br /></h2>
            <p>Pentru că vrem să îți placă comunitatea noastră, ți-am pregătit concursuri săptămânale unde poți câștiga premii pe măsură. Invață și căștigă! Fotografiază și diztrează-te!</p>
        </div>
    </section>
</section>

<!-- Three -->
<section id="three" class="wrapper style3 special">
    <div class="inner">
        <header class="major">
            <h2>Photo ART</h2>
            <p>Utilizand platforma noastra vei descoperi<br /> noi orizonturi vis a vis de cariera de fotograf</p>
        </header>
        <ul class="features">
            <li class="icon fa-paper-plane">
                <a href="{{ route('courses') }}">
                    <h3>Cursuri online</h3>
                </a>

                {{-- <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p> --}}
            </li>
            <li class="icon solid fa-images">
                <a href="{{ route('gallery') }}">
                    <h3>Galerie</h3>
                </a>
                {{-- <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p> --}}
            </li>
            <li class="icon solid fa-code">
                <a href="{{ route('weekly') }}">
                    <h3>Weekly Challenge</h3>
                </a>
                {{-- <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p> --}}
            </li>
            <li class="icon solid fa-euro-sign">
                <a href="{{ route('sales') }}">
                    <h3>Vânzări</h3>
                </a>
                {{-- <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p> --}}
            </li>
            <li class="icon solid fa-tv">
                <a href="{{ route('workshop') }}">
                    <h3>Workshop</h3>
                </a>
                {{-- <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p> --}}
            </li>
            <li class="icon solid fa-rss">
                <a href="{{ route('blog') }}">
                    <h3>Blog</h3>
                </a>
                {{-- <p>Augue consectetur sed interdum imperdiet et ipsum. Mauris lorem tincidunt nullam amet leo Aenean ligula consequat consequat.</p> --}}
            </li>
        </ul>
    </div>
</section>

<!-- CTA -->
<section id="cta" class="wrapper style4">
    <div class="inner">

        <form action="{{route('contact-us')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="row gtr-uniform">
                <div class="col-6 col-12-xsmall">
                    {!! Form::label('name', 'Nume') !!}
                    {!! Form::text('name', null) !!}
                </div>

                <div class="col-6 col-12-xsmall">
                    {!! Form::label('email', 'Email') !!}
                    {!! Form::email('email', null) !!}
                </div>
                <div class="col-12">
                    {!! Form::label('message', 'Messaj:') !!}
                    {!! Form::textarea('message', null) !!}
                </div>
                <div class="col-12">
                    <ul class="actions">
                        <li><input type="submit" value="Send Message" class="primary" /></li>
                        <li><input type="reset" value="Reset" /></li>
                    </ul>
                </div>

            </div>
        </form>
    </div>
</section>

@endsection
