    <!-- Header -->
    <header id="header" class="alt">
        <h1><a href="{{ route('intro') }}">Photo Art</a></h1>
        <nav id="nav">
            <ul>
                <li class="special">
                    <a href="#menu" class="menuToggle"><span>Menu
                            <i class="fas fa-bars"></i>
                        </span></a>
                    <div id="menu">
                        <ul>
                            <li><a href=""><i class="fas fa-close"></i></a></li>
                            <li><a href="{{ route('intro') }}">Home</a></li>
                            <li><a href="{{ route('blog') }}">Blog</a></li>
                            <li><a href="{{ route('workshop') }}">Workshop</a></li>
                            <li><a href="{{ route('courses') }}">Cursuri online</a></li>
                            <li><a href="{{ route('gallery') }}">Galerie</a></li>
                            <li><a href="{{ route('weekly') }}">Weekly Challenge</a></li>
                            <li><a href="{{ route('sales') }}">Vânzări</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </nav>
    </header>