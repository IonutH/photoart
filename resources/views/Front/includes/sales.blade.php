@extends('Front.layouts.index')

@section('content')

<!-- Main -->
<article id="main" class="sales">
    <header>
        <h2>Vanzari</h2>
        <p>Explorand aceasta opere vei putea descoperi frumuseti nebanuite</p>
    </header>
    <section class="wrapper style5">
        <div class="container">
            <div class="lightboxgallery-gallery clearfix">
                @foreach($sales as $item)
                <a class="lightboxgallery-gallery-item" id="{{$item-> id}}" target="_blank" href="{{asset('images/frontImages/sales/' . $item->image)}}"
                    data-title="{{$item->description}}" data-alt="{{$item->description}}" data-desc="Postat: {{$item->created_at->format('d/m/Y')}}">
                    <div>
                        <img src="{{asset('images/frontImages/sales/' . $item->image)}}">
                        <div class="lightboxgallery-gallery-item-content">
                            <span class="lightboxgallery-gallery-item-title">
                                <div class="card-details" style="text-align: center">
                                    <div class="price">
                                        {{ $item->price }} <i class="fas fa-euro-sign" aria-hidden="true"></i>
                                    </div>
                                </div>
                            </span>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
    </section>
</article>

@endsection