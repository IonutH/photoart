@extends('Front.layouts.index')

@section('content')

<!-- Main -->
<article id="main" class="weekly">
    <header>
        <h2>Weekly Challenge</h2>
        <p>Fiecare saptamana vine cu provocarea ei!</p>
    </header>
    <section class="wrapper style5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="current-challenge">
                        <h3 class="mb-5">This week's challenge</h3>
                        <p> Temă: {{$curentWeeklyChallenge->title}} </p>
                        <p> Descriere: {{$curentWeeklyChallenge->description}} </p>
                        <img src="{{asset('images/frontImages/weekly/' . $curentWeeklyChallenge->image)}}" alt="{{$curentWeeklyChallenge->title}}" width="500px">
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <h3 class="text-center mb-5">Post you're challenge</h3>
                    <form action="{{route('uploadWeekly')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            {!! Form::label('title', 'Titlu:') !!}
                            {!! Form::text('title', null, ['class' => 'form-control']) !!}
                        </div>
                        <div class="form-group">
                            <label for="image">Imagine concurs</label>
                            <input type="file" name="image" class="form-control">
                        </div>

                        <div class="form-group">
                            {!! Form::label('description', 'Descriere:') !!}
                            {!! Form::textarea('description', null, ['class' => 'form-control form-control-taxtarea']) !!}
                        </div>

                        <div class="form-group">
                            <button type="submit" class="primary">Adaugă concurs</button>
                        </div>
                    </form>

                </div>
                <div class="col-12">
                    <hr>
                    <h3 class="text-center mb-5 mt-5">Gallery of submited challenges</h3>
                    <div class="lightboxgallery-gallery clearfix">

                        @foreach($weeklyPhoto as $item)
                        <a class="lightboxgallery-gallery-item" id="{{$item-> id}}" target="_blank" href="{{asset('images/frontImages/weeklyPhoto/' . $item->image)}}"
                            data-title="{{$item->title}}" data-alt="{{$item->title}}" data-desc="{{$item->description}}">
                            <div>
                                <img src="{{asset('images/frontImages/weeklyPhoto/' . $item->image)}}" title="{{$item->title}}" alt="{{$item->title}}">
                                <div class="lightboxgallery-gallery-item-content">
                                    <span class="lightboxgallery-gallery-item-title">
                                        <div class="card-details">
                                            <div class="votes-weekly" id="{{ $item-> id }}">
                                                <span id="{{ $item-> id }}"> {{ $item->votes }} </span>
                                                <i class="fa fa-heart" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
@endsection
