@extends('Front.layouts.index')

@section('content')

<!-- Main -->
<article id="main" class="workshop">
    <header>
        <h2>Workshop</h2>
        <p>Nu te opri niciodata din invatat!</p>
    </header>
    <section class="wrapper style5">
        <div class="inner">
            <div class="row">
                @foreach ($workshops as $item)
                <div class="col-md-6 mt-4">
                    <div class="card" style="margin:auto;">
                        <img class="card-img-top" src="{{asset('images/frontImages/workshop/' . $item->image)}}" alt="{{$item->title}}">
                        <div class="card-body">
                            <h4 class="card-title">{{$item->title}} </h4>
                            <p class="card-text">{{$item->description}}</p>
                            <p class="card-text"><i class="far fa-calendar-plus"></i> {{$item->date}}</p>
                            <p class="card-text"><i class="far fa-hand-point-right"></i> {{$item->count}}</p>
                            <ul class="actions">
                                <li><input type="submit" value="Particip!" class="primary increaseWorkshop" id="{{ $item-> id }}" />
                                    
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
                <div class="col-12 justify-content-center d-flex mt-3">
                    {{ $workshops->links() }}
                </div>
            </div>

        </div>
    </section>
</article>

@endsection