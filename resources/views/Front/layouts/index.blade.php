<!DOCTYPE HTML>
<html>

<head>
    <title>PhotoArt - photos with style</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/front/main-front.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/front/fontawesome-all.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/front/jquery.fancybox.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/front/jquery.fancybox.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/front/lightboxgallery.css') }}" />
    <link rel="stylesheet" href="{{ asset('/css/front/lightboxgallery-style.css') }}" />

    <noscript>
        <link rel="stylesheet" href="{{ '/css/front/noscript.css' }}" />
    </noscript>

</head>

<body class="landing is-preload">
    <div id="page-wrapper">
        @include('Front.includes.navigation')
        @yield('content')
        @include('Front.includes.footer')
    </div>

    <!-- Scripts -->
    <script src="{{ '/js/front/jquery.min.js' }}"></script>
    <script src="{{ '/js/front/jquery.scrollex.min.js' }}"></script>
    <script src="{{ '/js/front/jquery.scrolly.min.js' }}"></script>
    <script src="{{ '/js/front/jquery.fancybox.pack.js' }}"></script>
    <script src="{{ '/js/front/browser.min.js' }}"></script>
    <script src="{{ '/js/front/breakpoints.min.js' }}"></script>
    <script src="{{ '/js/front/lightboxgallery.js' }}"></script>
    <script src="{{ '/js/front/util.js' }}"></script>
    <script src="{{ '/js/front/main.js' }}"></script>
    <script src="{{ '/js/front/gallery.js' }}"></script>



</body>

</html>
