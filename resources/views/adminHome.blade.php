@extends('back_admin.layouts.back_layouts')

{{--@section('content')--}}
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-8">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">Dashboard</div>--}}
{{--                    <div class="card-body">--}}
{{--                        You are Admin.--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}

@section('content')
    <div id="admin-page">
        <div class="hello-message">
            <h1>Bine ai venit pe pagina de administrare <span class="bold"><span class="blue-word">K</span><span
                        class="green-word">U</span><span class="red-word">B</span><span
                        class="orange-word">Z</span></span>!
            </h1>
            <div class="logo-kubz">
                <img src="{{asset('images/logo-kubz.svg')}}" width="450px" alt="kubz.ro">
            </div>
        </div>
    </div>
@endsection
