@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Bine ai venit pe pagina de administrare</h1>
            </div>
            <div class="col-md-8 center">
                <h3>Locul unde pasiunea ta va deveni realitate</h3>
            </div>
        </div>
    </div>
    <div class="logo" style="margin: auto;">
        <img src="{{asset('img/LogoPhoto.png')}}" alt="photoart">
    </div>
@endsection
