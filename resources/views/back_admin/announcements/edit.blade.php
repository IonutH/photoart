@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza un articol</h1>

        <div class="row justify-content-center">
            {!! Form::model($blog, ['method' => 'PATCH', 'action' => ['BlogController@update', $blog->id], 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('title', 'Titlu:') !!}
                {!! Form::text('title', $blog->title, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('subtitle', 'Subtitlu:') !!}
                {!! Form::text('subtitle', $blog->subtitle, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', $blog->description, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category', 'Categorii:') !!}
                {!! Form::text('category', $blog->category, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('blog_image', 'Fotografie:') !!}
                <img src="{{asset('images/backImages/blog/' . $blog->blog_image)}}" alt="">
                {!! Form::file('blog_image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza un articol', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection

