@extends('back_admin.layouts.back_layouts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1>Anunțuri online</h1>
        </div>
        <div class="col-4" style="padding-left: 185px;padding-top: 10px;">
            <a class="btn btn-primary" href="{{route('announcements.create')}}">Adaugă un anunț</a>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Titlu</th>
                    {{--                <th>Imagine</th>--}}
                    <th>Descriere</th>
                    <th>Categorie</th>
                    <th>Șterge</th>
                    <th>Editează</th>
                </tr>
            </thead>
            <tbody>
                @if($announcements)
                @foreach($announcements as $announcement)
                <tr>
                    <td>{{$announcement->id}}</td>
                    <td>{{$announcement->title}}</td>
                    {{--                        <td><img src="{{asset('images/backImages/announcements/' . $announcement->image)}}" alt=""></td>--}}
                    <td>{{\Illuminate\Support\Str::limit($announcement->description, $limit = 200, $end = '...')}}</td>
                    <td>{{$announcement->category}}</td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['AnnouncementsController@destroy', $announcement->id]]) !!}

                        {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                        {!! Form::close() !!}
                    </td>
                    <td><a class="btn btn-primary" href="{{route('announcements.edit', $announcement->id)}}">Editează</a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <div class="col-12 justify-content-center d-flex mt-3 mb-3">
            {{ $announcements->links() }}
        </div>
    </div>
</div>
@endsection