@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Adaugă un articol în blog</h1>

        <div class="row justify-content-center">
            {!! Form::open(['method' => 'POST', 'action' => 'BlogController@store' , 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('title', 'Titlu:') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('subtitle', 'Subtitlu:') !!}
                {!! Form::text('subtitle', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category', 'Categorii:') !!}
                {!! Form::text('category', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('blog_image', 'Fotografie:') !!}
                {!! Form::file('blog_image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Adauga articolul', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
