@extends('back_admin.layouts.back_layouts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1>Blog - Articole</h1>
        </div>
        <div class="col-4" style="padding-left: 185px;padding-top: 10px;">
            <a class="btn btn-primary" href="{{route('blog.create')}}">Adauga un articol</a>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Titlu</th>
                    <th>Subtitlu</th>
                    <th>Descriere</th>
                    <th>Categorii</th>
                    <th>Imagine</th>
                    <th>Șterge</th>
                    <th>Editează</th>
                </tr>
            </thead>
            <tbody>
                @if($blog)
                @foreach($blog as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{$item->subtitle}}</td>
                    <td>{{\Illuminate\Support\Str::limit($item->description, $limit = 300, $end = '...')}}</td>
                    <td>{{$item->category}}</td>
                    <td><img src="{{asset('images/backImages/blog/' . $item->blog_image)}}" width="150px" alt=""></td>
                    {{--                        <td>{{$item->datails_id ? $item->details_id : ''}}</td>--}}
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['BlogController@destroy', $item->id]]) !!}

                        {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                        {!! Form::close() !!}
                    </td>
                    <td><a class="btn btn-primary" href="{{route('blog.edit', $item->id)}}">Editează</a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>

        <div class="col-12 justify-content-center d-flex mt-3">
            {{ $blog->links() }}
        </div>
    </div>

</div>
@endsection