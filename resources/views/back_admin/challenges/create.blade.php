@extends('back_admin.layouts.back_layouts')
@section('content')
    <div class="container">
        <h1 style="text-align: center">Adaugă un concurs săptămânal</h1>

        <div class="row justify-content-center">
            <form action="{{route('challenges.store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    {!! Form::label('title', 'Titlu:') !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    <label for="image">Imagine concurs</label>
                    <input type="file" name="image" class="form-control">
                </div>

                <div class="form-group">
                    {!! Form::label('description', 'Descriere:') !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-success">Adaugă concurs</button>
                </div>
            </form>
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
