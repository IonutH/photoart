@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza un articol</h1>

        <div class="row justify-content-center">
            {!! Form::model($weekly, ['method' => 'PATCH', 'action' => ['BlogController@update', $weekly->id], 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('title', 'Titlu:') !!}
                {!! Form::text('title', $weekly->title, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', $weekly->description, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category', 'Categorii:') !!}
                {!! Form::text('category', $weekly->category, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('blog_image', 'Fotografie:') !!}
                <img src="{{asset('images/backImages/blog/' . $weekly->image)}}" alt="">
                {!! Form::file('blog_image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza un challenge', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
