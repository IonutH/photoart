@extends('back_admin.layouts.back_layouts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1>Concurs - Weekly Challenge</h1>
        </div>
        <div class="col-4" style="padding-left: 185px;padding-top: 10px;">
            <a class="btn btn-primary" href="{{route('challenges.create')}}">Adauga un challenge</a>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Titlu</th>
                    <th>Descriere</th>
                    <th>Imagine</th>
                    <th>Șterge</th>
                    <th>Editează</th>
                </tr>
            </thead>
            <tbody>
                @if($weekly)
                @foreach($weekly as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{$item->description}}</td>
                    <td><img src="{{asset('images/backImages/weekly/' . $item->image)}}" width="150px" alt=""></td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['WeeklyChallengeController@destroy', $item->id]]) !!}

                        {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                        {!! Form::close() !!}
                    </td>
                    <td><a class="btn btn-primary" href="{{route('challenges.edit', $item->id)}}">Editează</a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <div class="col-12 justify-content-center d-flex mt-3 mb-3">
            {{ $weekly->links() }}
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-8">
            <h1>Concurs - Fotografii Weekly Challenge</h1>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th>Titlu</th>
                    <th>Descriere</th>
                    <th>Imagine</th>
                    <th>Voturi</th>
                </tr>
            </thead>
            <tbody>
                @if($photos)
                @foreach($photos as $item)
                <tr>
                    <td>{{$item->title}}</td>
                    <td>{{$item->description}}</td>
                    <td><img src="{{asset('images/backImages/weeklyPhoto/' . $item->image)}}" width="150px" alt=""></td>
                    <td>{{$item->votes}}</td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['WeeklyChallengeController@destroyPhoto', $item->id]]) !!}

                        {!! Form::submit('Descalifica', ['class' => 'btn btn-danger close-icon']) !!}

                        {!! Form::close() !!}
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <div class="col-12 justify-content-center d-flex mt-3">
            {{ $photos->links() }}
        </div>
    </div>
</div>
@endsection