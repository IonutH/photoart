@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Adauga un curs online</h1>

        <div class="row justify-content-center">
            {!! Form::open(['method' => 'POST', 'action' => 'CoursesController@store' , 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('title', 'Titlu curs online:') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere curs') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category', 'Categorii:') !!}
                {!! Form::text('category', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('video', 'Video:') !!}
                {!! Form::file('video', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Adauga cursul online', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
