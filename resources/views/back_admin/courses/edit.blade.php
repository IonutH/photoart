@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza un articol</h1>

        <div class="row justify-content-center">
            {!! Form::model($courses, ['method' => 'PATCH', 'action' => ['CoursesController@update', $courses->id], 'enctype' => 'multipart/form-data']) !!}


            <div class="form-group">
                {!! Form::label('title', 'Titlu curs:') !!}
                {!! Form::text('title', $courses->title, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere curs:') !!}
                {!! Form::textarea('description', $courses->description, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('video', 'Video curs:') !!}
{{--                <video src="{{asset('videos/' . $courses->video)}}">--}}
                {!! Form::file('video', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza cursul', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
