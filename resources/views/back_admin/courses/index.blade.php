@extends('back_admin.layouts.back_layouts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1>Cursuri online</h1>
        </div>
        <div class="col-4" style="padding-left: 185px;padding-top: 10px;">
            <a class="btn btn-primary" href="{{route('courses.create')}}">Adauga un curs</a>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Titlu</th>
                    <th>Descriere</th>
                    <th>Șterge</th>
                    <th>Editează</th>
                </tr>
            </thead>
            <tbody>
                @if($courses)
                {{--                {{dd($courses)}}--}}
                @foreach($courses as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{$item->description}}</td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['CoursesController@destroy', $item->id]]) !!}

                        {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                        {!! Form::close() !!}
                    </td>
                    <td><a class="btn btn-primary" href="{{route('courses.edit', $item->id)}}">Editează</a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <div class="col-12 justify-content-center d-flex mt-3">
            {{ $courses->links() }}
        </div>
    </div>
</div>
@endsection