@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Adaugă o fotografie în galerie</h1>

        <div class="row justify-content-center">
            {!! Form::open(['method' => 'POST', 'action' => 'GalleryController@store' , 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('title', 'Titlu:') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('tags', 'Categorii:') !!}
                {!! Form::text('tags', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('iso_id', 'ISO:') !!}
                {!! Form::select('iso_id', ['' => 'Alege ISO'] + $iso, null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('camera_id', 'Camera:') !!}
                {!! Form::select('camera_id', ['' => 'Alege camera'] + $camera, null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('exposure_id', 'Expunere:') !!}
                {!! Form::select('exposure_id', ['' => 'Alege expunerea'] + $exposure, null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('lens_id', 'Obiectiv:') !!}
                {!! Form::select('lens_id', ['' => 'Alege obiectiv'] + $lens, null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('diaphragm_id', 'Diafragma:') !!}
                {!! Form::select('diaphragm_id', ['' => 'Alege diafragma'] + $diaphragm, null, ['class' => 'form-control']) !!}
            </div>


            <div class="form-group">
                {!! Form::label('image', 'Fotografie:') !!}
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Adauga fotografia', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
