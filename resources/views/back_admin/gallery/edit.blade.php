@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editează o fotografie din galerie</h1>

        <div class="row justify-content-center">
            {!! Form::model($gallery, ['method' => 'PATCH', 'action' => ['GalleryController@update', $gallery->id], 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('title', 'Titlu:') !!}
                {!! Form::text('title', $gallery->title, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', $gallery->description, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('tags', 'Categorii:') !!}
                {!! Form::text('tags', $gallery->tags, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('iso_id', 'ISO:') !!}
                {!! Form::select('iso_id', ['' => 'Alege ISO'] + $iso, $gallery->iso_id, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('camera_id', 'Camera:') !!}
                {!! Form::select('camera_id', ['' => 'Alege camera'] + $camera, $gallery->camera_id, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('exposure_id', 'Expunere:') !!}
                {!! Form::select('exposure_id', ['' => 'Alege expunerea'] + $exposure, $gallery->exposure_id, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('lens_id', 'Obiectiv:') !!}
                {!! Form::select('lens_id', ['' => 'Alege obiectiv'] + $lens, $gallery->lens_id, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('diaphragm_id', 'Diafragma:') !!}
                {!! Form::select('diaphragm_id', ['' => 'Alege diafragma'] + $diaphragm, $gallery->diaphragm_id, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('image', 'Fotografie:') !!}
                <img src="{{asset('images/backImages/gallery/' . $gallery->image)}}" alt="">
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza o fotografie', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
