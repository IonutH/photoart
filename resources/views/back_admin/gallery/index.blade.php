@extends('back_admin.layouts.back_layouts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1>Galerie foto</h1>
        </div>
        <div class="col-4" style="padding-left: 185px;padding-top: 10px;">
            <a class="btn btn-primary" href="{{route('gallery.create')}}">Adaugă o fotografie</a>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Titlu</th>
                    <th>Descriere</th>
                    <th>Imagine</th>
                    <th>Categorii</th>
                    <th>Șterge</th>
                    <th>Editează</th>
                </tr>
            </thead>
            <tbody>
                @if($gallery)
                @foreach($gallery as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td>{{$item->title}}</td>
                    <td>{{\Illuminate\Support\Str::limit($item->description, $limit = 300, $end = '...')}}1</td>
                    <td><img src="{{asset('images/backImages/gallery/' . $item->image)}}" width="150px" alt=""></td>
                    <td>{{$item->tags}}</td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['GalleryController@destroy', $item->id]]) !!}

                        {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                        {!! Form::close() !!}
                    </td>
                    <td><a class="btn btn-primary" href="{{route('gallery.edit', $item->id)}}">Editează</a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>

        <div class="col-12 justify-content-center d-flex mt-3">
            {{ $gallery->links() }}
        </div>

    </div>
</div>
@endsection