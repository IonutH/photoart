<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion text-center" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Panou Administrativ</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{route('admin.home')}}" aria-expanded="false">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    {{-- <div class="sidebar-heading">
        Interface
    </div> --}}

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('gallery.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-cog"></i>
            <span>Galerie Foto</span>
        </a>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link" href="{{route('details.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Detalii tehnice</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('blog.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Articole</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('courses.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Cursuri online</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('workshop.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Workshops</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('challenges.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Weekly Challenge</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('announcements.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Anunțuri Online</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href="{{route('sales.index')}}" aria-expanded="false">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Vanzări produse / </span>
            <span>Licitații</span>
        </a>
    </li>

    {{-- <li class="nav-item">
            <i class="fas fa-fw fa-wrench"></i>
            <span>Utilizatori</span>
        </a>
    </li> --}}

    <!-- Divider -->
    <hr class="sidebar-divider">
</ul>
<!-- End of Sidebar -->




