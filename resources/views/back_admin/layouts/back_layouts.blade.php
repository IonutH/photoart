<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>PhotoArt - Administrator</title>

    <link href="{{asset('fontawesome-free/css/all.min.css" rel="stylesheet')}}" type="text/css">
    <link href="{{asset('css/back/back.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i')}}" rel="stylesheet">
    <link href="{{asset('css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">
<div id="wrapper">
    @include('back_admin.includes.sidebar')
    <div id="content-wrapper" class="d-flex flex-column">
        <div id="content">
            @include('back_admin.includes.navigation')
            <main>
                @yield('content')
            </main>
        </div>
    </div>
</div>


<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<script src="{{asset('jquery-easing/jquery.easing.min.js')}}"></script>

<script src="{{asset('js/sb-admin-2.min.js')}}"></script>

<script src="{{asset('chart.js/Chart.min.js')}}"></script>

<script src="{{asset('js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('js/demo/chart-pie-demo.js')}}"></script>
@yield('page-script')
</body>
</html>
