@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Adaugă un produs la vânzare</h1>

        <div class="row justify-content-center">
            {!! Form::open(['method' => 'POST', 'action' => 'SalesController@store' , 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category', 'Categorii:') !!}
                {!! Form::text('category', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('image', 'Fotografie:') !!}
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('price', 'Preț - Euro:') !!}
                {!! Form::text('price', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Adaugă produsul', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
