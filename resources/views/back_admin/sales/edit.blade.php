@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza un articol</h1>

        <div class="row justify-content-center">
            {!! Form::model($sales, ['method' => 'PATCH', 'action' => ['SalesController@update', $sales->id], 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', $sales->description, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('category', 'Categorii:') !!}
                {!! Form::text('category', $sales->category, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('image', 'Fotografie:') !!}
                <img src="{{asset('images/backImages/sales/' . $sales->image)}}" alt="">
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('price', 'Pret - Euro:') !!}
                {!! Form::text('price', $sales->subtitle, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza produsul', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
