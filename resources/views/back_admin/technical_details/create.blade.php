@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Adauga detalii tehnice</h1>

        <div class="row justify-content-center">
            {!! Form::open(['method' => 'POST', 'action' => 'PhotoDetailsController@store']) !!}

            <div class="form-group">
                {!! Form::label('iso', 'ISO:') !!}
                {!! Form::text('iso', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('camera', 'Camera') !!}
                {!! Form::text('camera', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('exposure', 'Expunere:') !!}
                {!! Form::text('exposure', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('lens', 'Obiectiv:') !!}
                {!! Form::text('lens', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('diaphragm', 'Diafragma:') !!}
                {!! Form::text('diaphragm', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Adauga detalii', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
