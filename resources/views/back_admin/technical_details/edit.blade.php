@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza detaliu tehnice</h1>

        <div class="row justify-content-center">
            {!! Form::model($details, ['method' => 'PATCH', 'action' => ['PhotoDetailsController@update', $details->id]]) !!}

            <div class="form-group">
                {!! Form::label('iso', 'ISO:') !!}
                {!! Form::text('iso', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza detaliu', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
