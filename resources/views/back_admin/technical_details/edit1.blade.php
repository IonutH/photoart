@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza camera</h1>

        <div class="row justify-content-center">
            {!! Form::model($details1, ['method' => 'PUT', 'action' => ['PhotoDetailsController@update1', $details1->id]]) !!}

            <div class="form-group">
                {!! Form::label('camera', 'Camera') !!}
                {!! Form::text('camera', $details1->camera, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza detaliu', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>
    </div>
@endsection
