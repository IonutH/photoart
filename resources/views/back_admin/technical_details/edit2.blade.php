@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza expunerea</h1>

        <div class="row justify-content-center">
            {!! Form::model($details2, ['method' => 'PUT', 'action' => ['PhotoDetailsController@update2', $details2->id]]) !!}

            <div class="form-group">
                {!! Form::label('exposure', 'Expunerea:') !!}
                {!! Form::text('exposure', $details2->exposure, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza detaliu', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
