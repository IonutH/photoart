@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza obiectivul</h1>

        <div class="row justify-content-center">
            {!! Form::model($details3, ['method' => 'PUT', 'action' => ['PhotoDetailsController@update1', $details3->id]]) !!}

            <div class="form-group">
                {!! Form::label('lens', 'Obiectiv:') !!}
                {!! Form::text('lens', $details3->camera, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza detaliu', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
