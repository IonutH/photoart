@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza diafragma</h1>

        <div class="row justify-content-center">
            {!! Form::model($details4, ['method' => 'PUT', 'action' => ['PhotoDetailsController@update4', $details4->id]]) !!}

            <div class="form-group">
                {!! Form::label('diaphragm', 'Diafragma:') !!}
                {!! Form::text('diaphragm', $details4->camera, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza detaliu', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
