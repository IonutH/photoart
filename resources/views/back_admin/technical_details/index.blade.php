@extends('back_admin.layouts.back_layouts')

@section('content')
<div class="container" id="details">
    <div class="row">
        <div class="col-8">
            <h1>Detaliile Tehnice ale unei fotografii</h1>
        </div>
        <div class="col-4" style="padding-left: 185px;padding-top: 10px;">
            <a class="btn btn-primary" href="{{route('details.create')}}">Adauga un detaliu</a>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>ISO</th>
                <th>Șterge</th>
                <th>Editează</th>
            </tr>
        </thead>
        <tbody>
            @if($details)
            @foreach($details as $detail)
            <tr>
                <td>{{$detail->id}}</td>
                <td>{{Str::limit($detail-> iso, $limit = 27, $end = '...')}}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'action' => ['PhotoDetailsController@destroy', $detail->id]]) !!}

                    {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                    {!! Form::close() !!}
                </td>
                <td><a class="btn btn-primary" href="{{route('details.edit', $detail->id)}}">Editează</a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

    <div class="col-12 justify-content-center d-flex mt-3 mb-3">
        {{ $details->links() }}
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Camera</th>
                <th>Șterge</th>
                <th>Editează</th>
            </tr>
        </thead>
        <tbody>
            @if($details1)
            @foreach($details1 as $detail)
            <tr>
                <td>{{$detail->id}}</td>
                <td>{{Str::limit($detail-> camera, $limit = 27, $end = '...')}}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'action' => ['PhotoDetailsController@destroy1', $detail->id]]) !!}

                    {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                    {!! Form::close() !!}
                </td>
                <td><a class="btn btn-primary" href="{{route('details.edit1', $detail->id)}}">Editează</a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

    <div class="col-12 justify-content-center d-flex mt-3 mb-3">
        {{ $details1->links() }}
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Exposure</th>
                <th>Șterge</th>
                <th>Editează</th>
            </tr>
        </thead>
        <tbody>
            @if($details2)
            @foreach($details2 as $detail)
            <tr>
                <td>{{$detail->id}}</td>
                <td>{{Str::limit($detail-> exposure, $limit = 27, $end = '...')}}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'action' => ['PhotoDetailsController@destroy2', $detail->id]]) !!}

                    {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                    {!! Form::close() !!}
                </td>
                <td><a class="btn btn-primary" href="{{route('details.edit2', $detail->id)}}">Editează</a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

    <div class="col-12 justify-content-center d-flex mt-3 mb-3">
        {{ $details2->links() }}
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Obiectiv</th>
                <th>Șterge</th>
                <th>Editează</th>
            </tr>
        </thead>
        <tbody>
            @if($details3)
            @foreach($details3 as $detail)
            <tr>
                <td>{{$detail->id}}</td>
                <td>{{Str::limit($detail-> lens, $limit = 27, $end = '...')}}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'action' => ['PhotoDetailsController@destroy3', $detail->id]]) !!}

                    {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                    {!! Form::close() !!}
                </td>
                <td><a class="btn btn-primary" href="{{route('details.edit3', $detail->id)}}">Editează</a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

    <div class="col-12 justify-content-center d-flex mt-3 mb-3">
        {{ $details3->links() }}
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Diafragma</th>
                <th>Șterge</th>
                <th>Editează</th>
            </tr>
        </thead>
        <tbody>
            @if($details4)
            @foreach($details4 as $detail)
            <tr>
                <td>{{$detail->id}}</td>
                <td>{{Str::limit($detail-> diaphragm, $limit = 27, $end = '...')}}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE', 'action' => ['PhotoDetailsController@destroy4', $detail->id]]) !!}

                    {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                    {!! Form::close() !!}
                </td>
                <td><a class="btn btn-primary" href="{{route('details.edit4', $detail->id)}}">Editează</a></td>
            </tr>
            @endforeach
            @endif
        </tbody>
    </table>

    <div class="col-12 justify-content-center d-flex mt-3 mb-3">
        {{ $details4->links() }}
    </div>

</div>
@endsection