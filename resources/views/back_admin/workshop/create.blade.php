@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Adaugă un workshop</h1>

        <div class="row justify-content-center">
            {!! Form::open(['method' => 'POST', 'action' => 'WorkshopController@store' , 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('image', 'Fotografie:') !!}
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('title', 'Titlu:') !!}
                {!! Form::text('title', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere') !!}
                {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('date', 'Anul - Luna - Ziua Ora / Interval:') !!}
                {!! Form::text('date', null, ['class' => 'form-control', 'id' => 'datePick', 'autocomplete' => 'off']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Adauga articolul', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
