@extends('back_admin.layouts.back_layouts')

@section('content')
    <div class="container">
        <h1 style="text-align: center">Editeaza un workshop</h1>

        <div class="row justify-content-center">
            {!! Form::model($workshop, ['method' => 'PATCH', 'action' => ['WorkshopController@update', $workshop->id], 'enctype' => 'multipart/form-data']) !!}

            <div class="form-group">
                {!! Form::label('image', 'Fotografie workshop:') !!}
                <img src="{{asset('images/backImages/workshop/' . $workshop->image)}}" alt="">
                {!! Form::file('image', ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('title', 'Titlu:') !!}
                {!! Form::text('title', $workshop->title, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description', 'Descriere:') !!}
                {!! Form::textarea('description', $workshop->description, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('date', 'Anul - Luna - Ziua Ora / Interval:') !!}
                {!! Form::text('date', $workshop->category, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Actualizeaza un workshop', ['class' => 'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}
        </div>

        <div class="row">
            @include('back_admin.includes.errors')
        </div>

    </div>
@endsection
