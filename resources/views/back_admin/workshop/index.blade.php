@extends('back_admin.layouts.back_layouts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <h1>Workshop</h1>
        </div>
        <div class="col-4" style="padding-left: 185px;padding-top: 10px;">
            <a class="btn btn-primary" href="{{route('workshop.create')}}">Adaugă un workshop</a>
        </div>


        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Imagine</th>
                    <th>Titlu</th>
                    <th>Descriere</th>
                    <th>Data - An/luna/zi ora</th>
                    <th>Șterge</th>
                    <th>Editează</th>
                </tr>
            </thead>
            <tbody>
                @if($workshop)
                @foreach($workshop as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td><img src="{{asset('images/backImages/workshop/' . $item->image)}}" width="150px" alt=""></td>
                    <td>{{$item->title}}</td>
                    <td>{{\Illuminate\Support\Str::limit($item->description, $limit = 200, $end = '...')}}</td>
                    <td>{{$item->date}}</td>
                    <td>
                        {!! Form::open(['method' => 'DELETE', 'action' => ['WorkshopController@destroy', $item->id]]) !!}

                        {!! Form::submit('Șterge', ['class' => 'btn btn-danger close-icon']) !!}

                        {!! Form::close() !!}
                    </td>
                    <td><a class="btn btn-primary" href="{{route('workshop.edit', $item->id)}}">Editează</a></td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <div class="col-12 justify-content-center d-flex mt-3">
            {{ $workshop->links() }}
        </div>
    </div>
</div>
@endsection