@extends('back_user.layouts.back_layouts')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Bine ai venit pe pagina de utilizator</h1>
            </div>
        </div>
    </div>
    <div class="logo">
        <img src="{{asset('img/LogoPhoto.png')}}" alt="kubz.ro">
    </div>
@endsection
