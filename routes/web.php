<?php

use App\Http\Middleware\IsAdmin;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//admin pages
Route::get('admin/home', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::resource('/admin/details', 'PhotoDetailsController')->middleware('is_admin');
Route::resource('/admin/announcements', 'AnnouncementsController')->middleware('is_admin');
Route::resource('/admin/challenges', 'WeeklyChallengeController')->middleware('is_admin');
Route::resource('/admin/gallery', 'GalleryController')->middleware('is_admin');
Route::resource('/admin/blog', 'BlogController')->middleware('is_admin');
Route::resource('/admin/sales', 'SalesController')->middleware('is_admin');
Route::resource('/admin/workshop', 'WorkshopController')->middleware('is_admin');
Route::resource('/admin/courses', 'CoursesController')->middleware('is_admin');


//Route::resource('admin/blog', 'BlogController');

//user pages
Route::get('/home' , [
    'uses' => 'HomeController@index',
    'as' => 'home'
]);

Route::get('/test', function () {
   return view('admin');
});

//site
Route::get('/', function () {
    return redirect()->route('intro');
})->name('index');

Route::get('/intro', function () {
    return view('Front.includes.intro');
})->name('intro');

Route::post('contact-us', [
    'uses'=>'WebController@contactUs',
    'as'=>'contact-us']
);

Route::get('/blog', [
    'uses' => 'WebController@paginateBlog',
    'as' => 'blog'
]);
Route::resource('blogArticle', 'BlogController');

Route::get('/sales', [
    'uses' => 'WebController@paginateSales',
    'as' => 'sales'
]);

Route::get('/workshop', [
    'uses' => 'WebController@paginateWorkshos',
    'as' => 'workshop'
]);

Route::get('/increaseWorkshop', [
    'uses' => 'WebController@increaseWorkshopParticipate',
    'as' => 'increaseWorkshop'
]);

Route::get('/courses', function () {
    return view('Front.includes.courses');
})->name('courses');

Route::get('/gallery', 'WebController@getAllImages')->name('gallery');

Route::get('/weekly', [
    'uses' => 'WebController@getCurentWeeklyChallenge',
    'as' => 'weekly'
]);

Route::get('/increaseLike', [
    'uses' => 'WebController@increaseLike',
    'as' => 'increaseLike'
]);

Route::get('/increaseView', [
    'uses' => 'WebController@increaseView',
    'as' => 'increaseView'
]);

Route::get('/increaseVoteChallenge', [
    'uses' => 'WebController@increaseVoteChallenge',
    'as' => 'increaseVoteChallenge'
]);

Route::post('/uploadWeekly', [
    'uses' => 'WebController@uploadWeekly',
    'as' => 'uploadWeekly'
]);

Route::get('/blog/searchBlog/{variable}',[
   'uses' => 'WebController@searchBlog',
   'as' => 'searchBlog'
]);



//admin-panel
Route::get('/admin/camera/{details}/edit', [
    'uses' => 'PhotoDetailsController@edit1',
    'as' => 'details.edit1'
]);
Route::put('/admin/camera/{details}', [
    'uses' => 'PhotoDetailsController@update1',
    'as' => 'details.update1'
]);
Route::delete('/admin/camera/{details}', [
    'uses' => 'PhotoDetailsController@destroy1',
    'as' => 'details.destroy1'
]);


Route::get('/admin/exposure/{details}/edit', [
    'uses' => 'PhotoDetailsController@edit2',
    'as' => 'details.edit2'
]);
Route::put('/admin/exposure/{details}', [
    'uses' => 'PhotoDetailsController@update2',
    'as' => 'details.update2'
]);
Route::delete('/admin/exposure/{details}', [
    'uses' => 'PhotoDetailsController@destroy2',
    'as' => 'details.destroy2'
]);

Route::get('/admin/lens/{details}/edit', [
    'uses' => 'PhotoDetailsController@edit3',
    'as' => 'details.edit3'
]);
Route::put('/admin/lens/{details}', [
    'uses' => 'PhotoDetailsController@update3',
    'as' => 'details.update3'
]);
Route::delete('/admin/lens/{details}', [
    'uses' => 'PhotoDetailsController@destroy3',
    'as' => 'details.destroy3'
]);

Route::get('/admin/diaphragm/{details}/edit', [
    'uses' => 'PhotoDetailsController@edit4',
    'as' => 'details.edit4'
]);
Route::put('/admin/diaphragm/{details}', [
    'uses' => 'PhotoDetailsController@update4',
    'as' => 'details.update4'
]);
Route::delete('/admin/diaphragm/{details}', [
    'uses' => 'PhotoDetailsController@destroy4',
    'as' => 'details.destroy4'
]);

Route::delete('/admin/photos/{details}', [
    'uses' => 'WeeklyChallengeController@destroyPhoto',
    'as' => 'weekly.destroyPhoto'
]);


Auth::routes();
